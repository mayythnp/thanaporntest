package com.codetest.thnp.vo;

public class Response<T> {

	private String status = "0";
	private String message;

	public Response() {

	}

	public Response(String message) {
		this.message = message;
	}
	
	public Response(T data) {
		this.data = data;
	}

	private T data;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
