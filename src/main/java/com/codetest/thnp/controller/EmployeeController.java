package com.codetest.thnp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.codetest.thnp.service.EmployeeService;
import com.codetest.thnp.vo.EmployeeVo;
import com.codetest.thnp.vo.Response;

@RestController
@RequestMapping("employees")
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;

	@RequestMapping(method = RequestMethod.GET)
	public List<EmployeeVo> getAllEmployees() {
		return employeeService.getAll();
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{employeeId}")
	public EmployeeVo getEmployeeById(@PathVariable("employeeId") Long employeeId) {
		EmployeeVo response = employeeService.findById(employeeId);
		if (response == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Employee Data NOT FOUND");
		}
		return response;
	}

	@RequestMapping(method = RequestMethod.POST)
	public EmployeeVo insertEmployee(@RequestBody EmployeeVo employeeVo) {
		EmployeeVo response = employeeService.insertEmployee(employeeVo);
		return response;
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/{employeeId}")
	public Response<EmployeeVo> updateEmployee(@PathVariable("employeeId") Long employeeId,
			@RequestBody EmployeeVo employeeVo) {
		employeeVo.setId(employeeId);
		EmployeeVo response = employeeService.updateEmployee(employeeVo);
		if (response == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource");
		}
		return new Response<EmployeeVo>(response);
	}

	@RequestMapping(method = RequestMethod.DELETE, path = "/{employeeId}")
	public Response<Void> deleteEmployee(@PathVariable("employeeId") Long employeeId) {
		employeeService.deleteById(employeeId);
		return new Response<Void>("deleted");
	}

}
