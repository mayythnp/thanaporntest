package com.codetest.thnp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.codetest.thnp.entity.EmployeeEntity;

public interface EmployeeRepository extends JpaRepository<EmployeeEntity, Long> {

}
