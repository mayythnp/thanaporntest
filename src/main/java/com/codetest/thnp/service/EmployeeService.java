package com.codetest.thnp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codetest.thnp.entity.EmployeeEntity;
import com.codetest.thnp.repository.EmployeeRepository;
import com.codetest.thnp.vo.EmployeeVo;

@Service
public class EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;

	public List<EmployeeVo> getAll() {
		Iterable<EmployeeEntity> employee = employeeRepository.findAll();
		List<EmployeeVo> voList = new ArrayList<EmployeeVo>();

		employee.forEach((EmployeeEntity item) -> {
			EmployeeVo vo = employeeToEmployeeVo(item);
			System.out.print(vo);
			voList.add(vo);
		});
		return voList;
	}

	public EmployeeVo findById(Long id) {
		Optional<EmployeeEntity> employee = employeeRepository.findById(id);
		if (employee.isPresent()) {
			return employeeToEmployeeVo(employee.get());
		}
		return null;
	}

	public EmployeeVo insertEmployee(EmployeeVo vo) {
		EmployeeEntity employeeEntity = new EmployeeEntity(vo.getFirstName(), vo.getLastName());
		employeeRepository.save(employeeEntity);
		return employeeToEmployeeVo(employeeEntity);
	}

	public EmployeeVo updateEmployee(EmployeeVo vo) {
		Optional<EmployeeEntity> employee = employeeRepository.findById(vo.getId());
		if (employee.isPresent()) {
			EmployeeEntity entity = employee.get();
			entity.setFirstName(vo.getFirstName());
			entity.setLastName(vo.getLastName());
			employeeRepository.save(entity);
			EmployeeVo returnVo = employeeToEmployeeVo(entity);
			return returnVo;
		}
		return null;
	}

	public void deleteById(Long id) {
		employeeRepository.deleteById(id);
	}

	private EmployeeVo employeeToEmployeeVo(EmployeeEntity item) {
		EmployeeVo vo = new EmployeeVo();
		vo.setId(item.getId());
		vo.setFirstName(item.getFirstName());
		vo.setLastName(item.getLastName());
		return vo;
	}
}
