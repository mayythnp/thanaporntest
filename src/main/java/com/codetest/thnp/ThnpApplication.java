package com.codetest.thnp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThnpApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThnpApplication.class, args);
	}

}
