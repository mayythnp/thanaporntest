package com.codetest.thnp.service;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.internal.verification.Times;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.codetest.thnp.entity.EmployeeEntity;
import com.codetest.thnp.repository.EmployeeRepository;
import com.codetest.thnp.vo.EmployeeVo;

@SpringBootTest
public class EmployeeServiceTest {

	@MockBean(name = "employeeRepository")
	private EmployeeRepository employeeRepositoryTest;

	@Autowired
	private EmployeeService employeeService;

	private Long id_1 = (long) 001;
	private Long id_2 = (long) 002;
	private Long id_3 = (long) 003;
	private Long id_4 = (long) 004;
	private Long id_5 = (long) 005;

	@Test
	public void testFindAllEmployee() {
		List<EmployeeEntity> dataList = new ArrayList<EmployeeEntity>();
		EmployeeEntity data_1 = new EmployeeEntity();
		data_1.setId(id_1);
		data_1.setFirstName("Buck");
		data_1.setLastName("Horse");
		dataList.add(data_1);

		EmployeeEntity data_2 = new EmployeeEntity();
		data_2.setId(id_2);
		data_2.setFirstName("Cookies");
		data_2.setLastName("Puppy");
		dataList.add(data_2);

		EmployeeEntity data_3 = new EmployeeEntity();
		data_3.setId(id_3);
		data_3.setFirstName("Rodeo");
		data_3.setLastName("Bull");
		dataList.add(data_3);

		given(employeeRepositoryTest.findAll()).willReturn(dataList);
		List<EmployeeVo> response = employeeService.getAll();
		assertEquals(3, response.size(), "Employees List Found ! ");
	}

	@Test
	public void testFindAllEmployeeNoDataFound() {
		List<EmployeeEntity> dataList = new ArrayList<EmployeeEntity>();
		given(employeeRepositoryTest.findAll()).willReturn(dataList);
		List<EmployeeVo> response = employeeService.getAll();
		assertEquals(0, response.size(), "No Employee Found");
	}

	@Test
	public void testFindEmployeeById() {
		EmployeeEntity data_1 = new EmployeeEntity();
		data_1.setId(id_1);
		data_1.setFirstName("Buck");
		data_1.setLastName("Horse");

		EmployeeEntity data_2 = new EmployeeEntity();
		data_2.setId(id_2);
		data_2.setFirstName("Cookies");
		data_2.setLastName("Puppy");

		Optional<EmployeeEntity> dataCheck1 = Optional.of(data_1);
		given(employeeRepositoryTest.findById(id_1)).willReturn(dataCheck1);
		List<EmployeeVo> response_1 = employeeService.getAll();
		assertNotNull(response_1);

		Optional<EmployeeEntity> dataCheck2 = Optional.of(data_2);
		given(employeeRepositoryTest.findById(id_2)).willReturn(dataCheck2);
		List<EmployeeVo> response_2 = employeeService.getAll();
		assertNotNull(response_2);

	}

	@Test
	public void testFindEmployeeIdNotFound() {
		Optional<EmployeeEntity> dataCheck1 = Optional.empty();
		given(employeeRepositoryTest.findById(id_1)).willReturn(dataCheck1);
		EmployeeVo response = employeeService.findById(id_1);
		assertNull(response);
	}

	@Test
	public void testInsertByEmployeeId() {
		EmployeeVo vo = new EmployeeVo();
		vo.setFirstName("firstName_inst");
		vo.setLastName("lastName_inst");

		EmployeeEntity entity = new EmployeeEntity();
		entity.setId(id_1);
		entity.setFirstName("firstName_inst");
		entity.setLastName("lastName_inst");

		given(employeeRepositoryTest.save(Mockito.any())).willReturn(entity);
		EmployeeVo response = employeeService.insertEmployee(vo);
		verify(employeeRepositoryTest, new Times(1)).save(Mockito.any());
		assertNotNull(response);

	}

	@Test
	public void testDeleteByEmployeeId() {
		employeeService.deleteById(id_5);
		verify(employeeRepositoryTest, new Times(1)).deleteById(id_5);
	}

	@Test
	public void testUpdateEmployeeById() {
		EmployeeEntity data = new EmployeeEntity();
		data.setId(id_3);
		data.setFirstName(" Sir Rodeo");
		data.setLastName("Bull");

		EmployeeVo dataVo = new EmployeeVo();
		dataVo.setId(id_3);
		dataVo.setFirstName("Sir Rodeo");
		dataVo.setLastName("Bull");

		Optional<EmployeeEntity> dataEntity = Optional.of(data);
		given(employeeRepositoryTest.findById(id_3)).willReturn(dataEntity);
		given(employeeRepositoryTest.save(data)).willReturn(data);

		EmployeeVo response = employeeService.updateEmployee(dataVo);
		verify(employeeRepositoryTest, new Times(1)).save(data);
	}

	@Test
	public void testUpdateEmpIdNotFound() {
		EmployeeVo vo = new EmployeeVo();
		vo.setId((long) 006);
		vo.setFirstName("Bettani");
		vo.setLastName("Mouse");

		Optional<EmployeeEntity> dataCheck = Optional.empty();
		given(employeeRepositoryTest.findById((long) 006)).willReturn(dataCheck);
		EmployeeVo response = employeeService.findById((long) 006);
		assertNull(response);
	}

}
