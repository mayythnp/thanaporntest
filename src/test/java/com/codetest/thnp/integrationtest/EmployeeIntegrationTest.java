package com.codetest.thnp.integrationtest;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import com.codetest.thnp.service.EmployeeServiceTest;
import com.codetest.thnp.vo.EmployeeVo;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EmployeeIntegrationTest {
	@LocalServerPort
	private int port;
	MockMvc mockMvc;
	@Autowired
	TestRestTemplate restTemplate;

	@Test
	public void testGetAllEmployee() {
		try {
			ResponseEntity<List> resp = restTemplate.getForEntity("http://LOCALHOST:8080/employees", List.class);
			assertEquals(6, resp.getBody().size());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testGetEmployeeId() {
		try {
			ResponseEntity<EmployeeVo> resp = restTemplate.getForEntity("http://LOCALHOST:8080/employees/1",
					EmployeeVo.class);
			assertNotNull(resp);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// @Test
	public void testInsertEmployee() {
		try {
			EmployeeVo vo = new EmployeeVo();
			vo.setFirstName("FN");
			vo.setLastName("LN");

			ResponseEntity<EmployeeVo> resp = restTemplate.postForEntity("http://LOCALHOST:8080/employees", vo,
					EmployeeVo.class);
			assertNotNull(resp.getBody());
			assertEquals(vo.getFirstName(), resp.getBody().getFirstName());
			assertEquals(vo.getLastName(), resp.getBody().getLastName());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testUpdateEmployeeById() {
		try {
			EmployeeVo vo = new EmployeeVo();
			vo.setId((long) 004);
			vo.setFirstName("FN_update");
			vo.setLastName("LN_update");

			restTemplate.put("http://LOCALHOST:8080/employees/4", vo);
			ResponseEntity<EmployeeVo> response = restTemplate.getForEntity("http://LOCALHOST:8080/employees/4",
					EmployeeVo.class);

			assertEquals(HttpStatus.OK, response.getStatusCode());
			assertEquals(vo.getId(), response.getBody().getId());
			assertEquals(vo.getFirstName(), response.getBody().getFirstName());
			assertEquals(vo.getLastName(), response.getBody().getLastName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// @Test
	public void testDeleteEmployeeById() {
		try {

			restTemplate.delete("http://LOCALHOST:8080/employees/5");
			ResponseEntity<EmployeeVo> response = restTemplate.getForEntity("http://LOCALHOST:8080/employees/5",
					EmployeeVo.class);
			assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}