package com.codetest.thnp.controller;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.internal.verification.Times;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import com.codetest.thnp.service.EmployeeService;
import com.codetest.thnp.vo.EmployeeVo;
import com.codetest.thnp.vo.Response;

@SpringBootTest
public class EmployeeTestController {

	@Autowired
	private EmployeeController employeeController;

	@MockBean(name = "employeeService")
	private EmployeeService employeeService;

	private Long id_1 = (long) 001;
	private Long id_2 = (long) 002;
	private Long id_3 = (long) 003;
	private Long id_4 = (long) 004;
	private Long id_5 = (long) 005;

	@Test
	public void testFindAll() {
		List<EmployeeVo> dataList = new ArrayList<EmployeeVo>();
		EmployeeVo data_1 = new EmployeeVo();
		data_1.setId(id_1);
		data_1.setFirstName("Buck");
		data_1.setLastName("Horse");
		dataList.add(data_1);

		EmployeeVo data_2 = new EmployeeVo();
		data_2.setId(id_2);
		data_2.setFirstName("Cookies");
		data_2.setLastName("Puppy");
		dataList.add(data_2);

		EmployeeVo data_3 = new EmployeeVo();
		data_3.setId(id_3);
		data_3.setFirstName("Rodeo");
		data_3.setLastName("Bull");
		dataList.add(data_3);

		given(employeeService.getAll()).willReturn(dataList);
		List<EmployeeVo> response = employeeController.getAllEmployees();
		assertEquals(3, response.size());
	}

	@Test
	public void testFindAllNoDataFound() {
		List<EmployeeVo> voList = new ArrayList<EmployeeVo>();
		given(employeeService.getAll()).willReturn(voList);
		List<EmployeeVo> response = employeeController.getAllEmployees();
		assertEquals(0, response.size());
	}

	@Test
	public void testFindEmployeeById() {
		EmployeeVo data_1 = new EmployeeVo();
		data_1.setId(id_1);
		data_1.setFirstName("Buck");
		data_1.setLastName("Horse");

		EmployeeVo dataCheck = data_1;
		given(employeeService.findById(id_1)).willReturn(dataCheck);
		EmployeeVo response = employeeController.getEmployeeById(id_1);
		verify(employeeService, new Times(1)).findById(id_1);
		assertNotNull(response);
	}

	@Test
	public void testGetEmployeeByIdNotFound() {
		given(employeeService.findById((long) 010)).willReturn(null);
		assertThrows(ResponseStatusException.class, () -> {
			EmployeeVo response = employeeController.getEmployeeById((long) 010);
		});
		verify(employeeService, new Times(1)).findById((long) 010);
	}

	@Test
	public void testInsertByEmployeeId() {
		EmployeeVo vo = new EmployeeVo();
		vo.setFirstName("testUpdate-First");
		vo.setLastName("testUpdate-Last");

		given(employeeService.insertEmployee(Mockito.any())).willReturn(vo);
		EmployeeVo response = employeeController.insertEmployee(vo);
		verify(employeeService, new Times(1)).insertEmployee(vo);
	}

	@Test
	public void testDeleteByEmployeeId() {
		employeeService.deleteById(id_5);
		verify(employeeService, new Times(1)).deleteById(id_5);
	}

	@Test
	public void testDeleteByEmployeeIdNotFound() {
		EmployeeVo vo = new EmployeeVo();
		vo.setId((long) 006);

		employeeService.deleteById((long) 006);
		verify(employeeService, new Times(1)).deleteById((long) 006);
	}

	@Test
	public void testUpdateEmployeeById() {

		EmployeeVo vo = new EmployeeVo();
		vo.setId(id_1);
		vo.setFirstName("UpdateEmployeeById");
		vo.setLastName("UpdateEmployeeById");

		given(employeeService.updateEmployee(vo)).willReturn(vo);
		Response<EmployeeVo> response = employeeController.updateEmployee(id_5, vo);
		verify(employeeService, new Times(1)).updateEmployee(vo);
	}

	@Test
	public void testUpdateEmpIdNotFound() {
		EmployeeVo vo = new EmployeeVo();
		vo.setFirstName("UpdateEmpIdNotFound");
		vo.setLastName("UpdateEmpIdNotFound");

		given(employeeService.findById((long) 010)).willReturn(null);
		assertThrows(ResponseStatusException.class, () -> {
			Response<EmployeeVo> response = employeeController.updateEmployee(((long) 010), vo);
		});
		verify(employeeService, new Times(1)).updateEmployee(vo);
	}
}
